package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.EnumSymbols;
import ru.edu.model.Symbol;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SymbolPriceServiceImplTest {

    private final String SYMBOL_BTC = EnumSymbols.BTCUSDP.toString();

    private final String URL = "https://api.binance.com/api/v3/ticker/price?symbol=";

    private SymbolPriceServiceImpl service;

    @Before
    public void setup()  {
        service = new SymbolPriceServiceImpl(URL, 10);
    }

    @Test
    public void testGetPrice() throws InterruptedException {

        Symbol symbol = service.getPrice(SYMBOL_BTC);
        Symbol symbolAgain = service.getPrice(SYMBOL_BTC);
        assertEquals(symbol.getTimeStamp(), symbolAgain.getTimeStamp());

        System.out.println("Ждем сброса кэша");
        Thread.sleep(11_000L);

        Symbol symbolNew = service.getPrice(SYMBOL_BTC);
        assertTrue(symbol.getTimeStamp().isBefore(symbolNew.getTimeStamp()));

    }

    @Test
    public void testGetSymbolOnTimePeriod() throws InterruptedException {

        Symbol symbol = service.getPrice(SYMBOL_BTC);

        Thread.sleep(1_000L);

        Symbol symbolNew = service.getPrice(SYMBOL_BTC);
        assertEquals(symbol.getTimeStamp(), symbolNew.getTimeStamp());

    }

    @Test
    public void testGetPriceOnTimePeriod() throws InterruptedException {

        Symbol symbol = service.getPrice(SYMBOL_BTC);

        Thread.sleep(1_000L);

        Symbol symbolNew = service.getPrice(SYMBOL_BTC);
        assertEquals(symbol.getPrice(), symbolNew.getPrice());

    }

}