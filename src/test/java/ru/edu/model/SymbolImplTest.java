package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static org.junit.Assert.*;

public class SymbolImplTest {
    private final String SYMBOL_NAME = "BTCUSDP";
    private final String PRICE = "37313.57000000";

    private BigDecimal comparePrice;

    private Instant timeStamp;

    private SymbolImpl symbol;

    @Before
    public void setup(){
        comparePrice = new BigDecimal(PRICE);
        timeStamp = Instant.now();
        symbol = new SymbolImpl(SYMBOL_NAME, new BigDecimal(PRICE));
    }

    @Test
    public void testSymbol(){
        assertEquals(SYMBOL_NAME, symbol.getSymbol());
    }

    @Test
    public void testTime(){
        assertEquals(timeStamp, symbol.getTimeStamp());
    }

    @Test
    public void testPrice(){
        assertEquals(comparePrice, symbol.getPrice());
    }

}