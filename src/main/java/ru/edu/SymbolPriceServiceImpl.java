package ru.edu;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SymbolPriceServiceImpl implements SymbolPriceService {
    /**
     * Время хранения данных в кэше.
     */
    private long differenceTime;

    /**
     * Путь api.
     */
    private String url;

    /**
     * Хранит данные о валютах.
     */
    private Map<String, Symbol> cacheSymbols;

    /**
     * @param apiUrl
     * @param time
     */
    public SymbolPriceServiceImpl(final String apiUrl, final long time) {
        url = apiUrl;
        differenceTime = time;
        cacheSymbols = Collections.synchronizedMap(new HashMap<>());
    }

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш,
     * с помощью которого данные будут обновляться не чаще,
     * чем раз в differenceTime.
     *
     * @param symbolName
     * @return экземпляр данных о валюте
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        Symbol symbol = null;

        if (cacheSymbols.containsKey(symbolName)) {
            symbol = cacheSymbols.get(symbolName);

            if (timePassed(symbol.getTimeStamp(), Instant.now())) {
                symbol = getSymbol(symbolName);
                cacheSymbols.put(symbolName, symbol);
            }

        } else {
            symbol = getSymbol(symbolName);
            cacheSymbols.put(symbolName, symbol);
        }

        return symbol;
    }

    /**
     * Метод формирует символ из данных,
     * полученных от сервера.
     *
     * @param symbolName
     * @return данные о валюте.
     */
    private Symbol getSymbol(final String symbolName) {
        ResponseEntity<String> tmp = getResponse(symbolName);
        JsonNode json = responseToJson(tmp);
        String price = getRootByPath(json, "price").asText();

        return new SymbolImpl(symbolName, new BigDecimal(price));
    }

    /**
     * Сравнивает разницу между временами.
     *
     * @param from
     * @param to
     * @return true/false
     */
    private boolean timePassed(final Instant from, final Instant to) {
        Duration res = getDifferentTime(from, to);
        if (res.getSeconds() >= differenceTime) {
            return true;
        }
        return false;
    }

    /**
     * Возвращает разницу в секундах.
     *
     * @param from
     * @param to
     * @return секунды
     */
    private Duration getDifferentTime(final Instant from, final Instant to) {
        Duration res = Duration.between(from, to);
        return res;
    }

    /**
     * Возвращает значения узла в json.
     *
     * @param root
     * @param path
     * @return значение узла
     */
    private JsonNode getRootByPath(final JsonNode root, final String path) {
        return root.path(path);
    }

    /**
     * JSON маппер ответа.
     *
     * @param response
     * @return json представление response
     */
    private JsonNode responseToJson(final ResponseEntity<String> response) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;

        try {
            root = mapper.readTree(response.getBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return root;
    }

    /**
     * Получение ответа от сервера.
     *
     * @param symbolName
     * @return ответ от сервера
     */
    private ResponseEntity<String> getResponse(final String symbolName) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response
                = restTemplate.getForEntity(url + symbolName, String.class);

        return response;
    }

}
