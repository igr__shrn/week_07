package ru.edu.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;


public class SymbolImpl implements Symbol, Serializable {

    /**
     * Код валюты.
     */
    private String symbol;

    /**
     * Цена валюты.
     */
    private BigDecimal price;

    /**
     * Время получения данных.
     */
    private Instant timeStamp;

    /**
     * @param symbolName
     * @param priceSymbol
     */
    public SymbolImpl(final String symbolName,
                      final BigDecimal priceSymbol) {
        this.symbol = symbolName;
        this.price = priceSymbol;
        timeStamp = Instant.now();
    }

    /**
     * Наименование валюты.
     *
     * @return название
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * Цена валюты.
     *
     * @return цена
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Время получения данных.
     *
     * @return
     */
    @Override
    public Instant getTimeStamp() {
        return timeStamp;
    }

}
