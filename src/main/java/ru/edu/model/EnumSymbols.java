package ru.edu.model;

public enum EnumSymbols {
    /**
     * Биткоин.
     */
    BTCUSDP,

    /**
     * Эфир.
     */
    ETHUSDP
}
