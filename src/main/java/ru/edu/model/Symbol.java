package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс информации о курсе обмена.
 */
public interface Symbol {
    /**
     *  Наименование валюты.
     *
     * @return название
     */
    String getSymbol();

    /**
     * Цена валюты.
     *
     * @return цена
     */
    BigDecimal getPrice();

    /**
     * Время получения данных.
     *
     * @return время
     */
    Instant getTimeStamp();
}
